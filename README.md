# Nest

Modulární administrační rozhraní, umožnuje volitelnou rozšiřitelnost administrace systému StudioArt CMS

#### Rozšiřitelnost
 - přímo distribuované Eggs dodané s rozhraním
 - vzdálená registrace z jiného systémového extendu do slotu
 ```
 nest.remote.egg
 ```
 
#### Vyžaduje
PHP 5.4+

##### Verze
1.1.0

#### Licence
[MIT]

[//]: #

[MIT]: <https://cs.wikipedia.org/wiki/Licence_MIT>