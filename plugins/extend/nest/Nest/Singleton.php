<?php

namespace Nest;

/**
 * Singleton class
 *
 * @author jDaněk <jdanek.eu>
 */
abstract class Singleton
{

    /**
     * Instance
     *
     * @var Singleton
     */
    protected static $instance;

    /**
     * Constructor
     *
     * @return void
     */
    protected function __construct()
    {
        
    }

    /**
     * Get instance
     *
     * @return Singleton
     */
    public final static function getInstance()
    {
        if (null === static::$instance)
        {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function __clone()
    {
        throw new Exception("Class can not be cloned");
    }

}
