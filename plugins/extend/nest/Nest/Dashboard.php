<?php

namespace Nest;

use Sunlight\Message;
use Nest\Core as NestCore;
use Nest\Template\Template;

class Dashboard
{

    private $nest;
    private $theme;
    private $config;

    /**
     * 
     * @param string $theme cesta k tematu (vcetne koncoveho lomitka)
     */
    function __construct($nest, $theme)
    {
        $this->nest = $nest;
        $this->theme = $theme;
        $this->config = $nest::$config;
    }

    /**
     * Ziskani vystupu eggu
     * 
     * @global array $_lang
     * @return string
     */
    private function getEgg()
    {
        global $_lang;

        if (_get('m'))
        {
            $current_egg = $this->nest->getCurrentEgg();
            if (null !== $current_egg && null !== $current_egg->script)
            {
                return require $current_egg->script;
            }
            return Message::warning($_lang['admin.moduleunavailable']);
        }
        elseif (isset($this->config['default_egg']) && $this->nest->eggExists($this->config['default_egg']))
        {
            _redirectHeader(NestCore::composeEggUrl($this->config['default_egg']));
            exit;
        }
    }

    /**
     * Vykresleni sablony
     * 
     * @global array $_lang
     * @return string
     */
    public function renderTemplate()
    {
        global $_lang;

        $desktop = new Template();
        return $desktop->renderToString($this->theme . 'layout.latte', [
                    'nestroot'    => _nestroot,
                    'theme'       => _nest_theme,
                    'description' => $_lang['nest']['description'],
                    'eggs'        => $this->nest->getEggs(),
                    'groups'      => $this->nest->getGroups(),
                    'egg'         => $this->getEgg(),
        ]);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->renderTemplate();
    }

}
