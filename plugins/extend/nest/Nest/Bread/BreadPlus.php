<?php

namespace Nest\Bread;

use Latte\Macros\MacroSet;
use Nest\Bread\AdminBread;
use Nest\Template\Template;
use Nest\Template\Translator;
use Sunlight\Database\Database;
use Sunlight\Message;

/**
 * Admin BREAD class extended
 *
 * @author jDaněk <jdanek.eu>
 */
abstract class BreadPlus extends AdminBread
{

    /** @var array */
    public $filter_columns_whitelist = [];

    /** @var array */
    public $filter_options = [];

    /** @var array */
    public $filter_errors = [];

    /**
     * Akce pro zmenu stavu polozek
     * @param array $params
     * @param array $action
     * @param AdminBread $bread
     * @return type
     */
    static public function switchAction(array $params, array $action, AdminBread $bread)
    {
        $messages = [];
        $trigger = "_{$params[0]}_{$bread->uid}";

        // verify changes
        if (!isset($action['changes']))
        {
            return [
                ['msg' => 'Array `changes` is not defined'],
                self::ACTION_ERR,
            ];
        }

        // auto-set template
        if (!isset($action['template']))
        {
            $action['template'] = $bread->actions[$params[0]]['template'] = $params[0];
        }

        /* ----- load data ----- */
        // verify ID
        if (!isset($params[1]))
        {
            return [
                ['msg' => 'Missing parameter 1 for ' . __METHOD__],
                self::ACTION_ERR,
            ];
        }

        // process ID
        $id = (int) $params[1];

        // load data
        $sql = $bread->formatSql("SELECT %columns% FROM `" . _prefix . "{$bread->table}` {$bread->tableAlias} WHERE {$bread->tableAlias}.{$bread->primary}=@id@", [
            'columns' => array_merge([$bread->primary], $action['extra_columns']),
            'id'      => $id,
        ]);
        $data = Database::queryRow($sql);
        if (false === $data)
        {
            return [null, self::ACTION_NOT_FOUND];
        }

        // process submit form
        if (isset($_POST[$trigger]))
        {
            // simple confirm
            $success = Database::update(_prefix . $bread->table, $bread->primary . "=" . $id, $action['changes']);

            // handle result
            if ($success)
            {
                return [
                    ['messages' => $messages],
                    self::ACTION_DONE,
                ];
            }
            else
            {
                $messages[] = [2, $GLOBALS['_lang']['global.error']];
            }
        }

        /* ----- render ----- */
        return [
            ['messages' => $messages],
            $bread->render($action['template'], [
                'data'           => $data,
                'self'           => $params['action'],
                'submit_text'    => $GLOBALS['_lang']['global.do'],
                'submit_trigger' => $trigger,
            ]),
        ];
    }

    /**
     * Sestaveni casti dotazu pro filtrovani polozek
     * @example tvar filtru v GETu: "&filter=sloupec/hodnota" nebo "&filter[]=sloupec/hodnota"
     * 
     * @param string|array|null $filter
     * @return string
     */
    public function itemFilter()
    {
        $filters = isset($_SESSION['filter']) ? $_SESSION['filter'] : null;

        $whitelist = array_flip($this->filter_columns_whitelist);
        $cond = [];

        // PHP 5.3 clocure fix
        $that = $this;
        $processFilter = function($filter) use(&$cond, $whitelist, &$that) {
            foreach ((array) $filter as $column => $value)
            {

                if (isset($whitelist[$column]))
                {
                    $cond[] = $column . "='" . Database::esc($value) . "'";
                }
                else
                {
                    $that->filter_errors[] = sprintf($GLOBALS ['_lang']['nest']['bread.filter.whitelist'], $column);
                }
            }
        };

        if (null !== $filters)
        {
            // empty whitelist
            if (0 === count($whitelist))
            {
                $this->filter_errors = $GLOBALS ['_lang']['nest']['bread.filter.empty.whitelist'];
                return "1";
            }

            foreach ($filters as $filter)
            {
                $processFilter($filter);
            }


            // kontrola neprazdneho filtru pred pouzitim
            if (count($cond) > 0)
            {
                return implode(' AND ', $cond);
            }
        }
        return "1";
    }

    /**
     * Sestaveni nabidky pro select filtru
     * 
     * @return string
     */
    public function itemFilterOptions()
    {
        $options = "";
        foreach ($this->filter_options as $name => $params)
        {
            $flip = array_flip($this->filter_columns_whitelist);

            if (isset($flip[$params[0]]))
            {
                $options.="<option value='{$params[0]}/{$params[1]}'>{$name}</option>\n";
            }
        }

        return $options;
    }

    /**
     * [OVERRIDE] Render action link
     *
     * Supported options in $extra:
     * -----------------------------
     * icon_full (0)    treat $icon as full path instead of filename in admin/images/icons 1/0
     * new_window (0)   add target="_blank" to the link 1/0
     * class (-)        class string to add to the link
     * is_url (0)       treat $action as full url and ignore $params and $prev 1/0
     *
     * @param string|null $icon    admin icon name (without extension) or null for no icon
     * @param string      $caption button caption
     * @param string      $action  the action
     * @param array|null  $params  action params
     * @param string|null $prev    previous action or null
     * @param array|null  $extra   extra options or null
     * @return null prints the result
     */
    public function renderLink($icon, $caption, $action, array $params = null, $prev = null, array $extra = null)
    {
        // pridani ikony akce
        if (null !== $icon)
        {
            $icon = _nestroot . 'Resources/images/icons/' . $icon . '.png';
            $extra = array_merge((array) $extra, ['icon_full' => true]);
        }
        // lokalizace akce sablony
        $caption = Translator::translate($caption);

        parent::renderLink($icon, "<span>{$caption}</span>", $action, $params, $prev, $extra);
    }

    /**
     * [OVERRIDE] Render template
     *
     * @param string     $_template template name
     * @param array|null $_params   template parameters
     * @return string
     */
    public function render($_template, array $_params = null)
    {
        // predani sebe do sablony ($this nelze pouzit, znamenalo by to class Template)
        $_params['bread'] = $this;
        $tpl = new Template();

        // vlastni makro pro pouzit SL funkci zacinajicich podtrzitkem
        $set = new MacroSet($tpl->getCompiler());
        $set->addMacro('sl', function($node, $writer) {
            return $writer->write('echo %node.args');
        });

        // preklad
        $tpl->addFilter('translate', function($arg) {
            return call_user_func([Translator::className(), 'translate'], $arg);
        });

        $render_output = "";
        if ('list' === $_template)
        {
            if (isset($_SESSION['filter']))
            {
                $render_output.=Message::info($GLOBALS['_lang']['nest']['bread.filter.active']);
            }

            if (sizeof($this->filter_errors) > 0)
            {
                $render_output.=Message::warning(_msgList($this->filter_errors, 'errors', null, true), true);
            }
        }

        $render_output.=$tpl->renderToString($this->resource('tpl', $_template, 'latte'), $_params);
        return $render_output;
    }

    /**
     * [OVERRIDE] List action
     *
     * @param array      $params
     * @param array      $action
     * @param AdminBread $bread
     * @return array
     */
    public static function listAction(array $params, array $action, AdminBread $bread)
    {
        $messages = [];
        $trigger = "_list_{$bread->uid}";

        /* ----- prepare query ----- */

        // format condition
        if ('1' !== $action['query_cond'])
        {
            $cond = $bread->formatSql($action['query_cond'], $action['query_cond_params']);
        }
        else
        {
            $cond = $action['query_cond'];
        }

        // format sql
        $sql = $bread->formatSql($action['query'], [
            'columns'     => $action['columns'],
            'table'       => '`' . $bread->formatTable($bread->table) . "`",
            'table_alias' => $bread->tableAlias,
            'cond'        => $cond,
        ]);

        // add order by
        if (!empty($action['query_orderby']))
        {
            $sql .= " ORDER BY {$action['query_orderby']}";
        }

        /* ----- init paginator ----- */

        if ($action['paginator'])
        {
            $total = Database::queryRow('SELECT COUNT(*) total FROM `' . $bread->formatTable($bread->table) . '` ' . $bread->tableAlias . ' WHERE ' . $cond);
            $paging = _resultPaging($params['url'], $action['paginator_size'], intval($total['total']));
            $sql .= " {$paging['sql_limit']}";
        }
        else
        {
            $paging = null;
        }

        /* ----- fetch data ----- */

        $q = Database::query($sql);
        if (false === $q)
        {
            return [null, self::ACTION_ERR];
        }

        $count = Database::size($q);
        $result = Database::rows($q);
        Database::free($q);

        if (isset($_POST[$trigger]))
        {
            // handler or simple delete
            if (null !== $action['handler'])
            {
                // use handler
                $callbackResult = call_user_func($action['handler'], [
                    'success'  => &$callbackResult,
                    'result'   => $result,
                    'params'   => $params,
                    'action'   => $action,
                    'bread'    => $bread,
                    'messages' => &$messages,
                ]);
            }

            // handler result
            if (true === $callbackResult)
            {
                $messages[] = [1, 'Item updated'];
            }
            else
            {
                $messages[] = [2, _msgList($callbackResult, 'errors'), null, true];
            }
        }

        // return
        return [
            ['messages' => $messages],
            $bread->render($action['template'], [
                'result'         => $result,
                'count'          => $count,
                'paging'         => $paging,
                'self'           => $params['action'],
                'submit_text'    => $GLOBALS['_lang']['global.savechanges'],
                'submit_trigger' => $trigger,
        ])];
    }

}
