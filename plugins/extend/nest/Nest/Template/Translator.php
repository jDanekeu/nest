<?php

namespace Nest\Template;

/**
 * TemplateTranslator
 *
 * @author jDaněk <jdanek.eu>
 */
class Translator
{

    /**
     * Static class
     */
    private function __construct()
    {
        
    }

    /**
     * @return system
     */
    public static function className()
    {
        return __CLASS__;
    }

    /**
     * @global array $_lang
     * @param string $text
     * @return string
     */
    public static function translate($text)
    {
        global $_lang;

        if (isset($_lang['bread'][mb_strtolower($text)]))
        {
            return $_lang['bread'][mb_strtolower($text)];
        }
        return $text;
    }

}
