<?php

namespace Nest\Template;

use Latte\Engine;

/**
 * Template class
 *
 * @author jDaněk <jdanek.eu>
 */
class Template extends Engine
{

    /** @var string */
    public static $cache_directory = '';

    /**
     * Renders template to output.
     * @return void
     */
    public function render($name, array $params = [])
    {
        // nastaveni cache adresare
        parent::setTempDirectory(self::$cache_directory);

        // vykresleni
        return parent::render($name, $params);
    }

    /**
     * Renders template to string.
     * @return string
     */
    public function renderToString($name, array $params = [])
    {
        // nastaveni cache adresare
        parent::setTempDirectory(self::$cache_directory);

        // vykresleni
        return parent::renderToString($name, $params);
    }

}
