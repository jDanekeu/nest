<?php

namespace Nest;

use SplFileInfo;
use FilesystemIterator;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

/**
 * File finder
 *
 * @author jDaněk <jdanek.eu>
 */
class FileFinder
{

    /** @var string */
    private $root;

    /** @var array */
    private $paths = [];

    /** @var array */
    private $exclude = [];

    /** @var array */
    private $name = [];

    /** @var array */
    private $extension = [];

    /** @var int */
    private $maxDepth = -1;

    /** @var array */
    private $files = [];

    /**
     * 
     * @param string $root root directory (without a trailing slash)
     * @param array $rules see method rules 
     * @param bool $autofind requires completed $rules
     */
    public function __construct($root, $rules = [], $autofind = false)
    {
        $path = $this->normalizePath($root);
        $this->root = rtrim($path, '/');

        $this->rules($rules);

        if (true === $autofind)
        {
            $this->find();
        }
    }

    /**
     * Pravidla pro hledani souboru
     * 
     *  * Mozne klice v $rules:
     * -----------------------
     * names            pole povolenych nazvu souboru
     * extensions       pole povolenych pripon
     * paths            pole povolenych cest
     * excludes         pole vyloucenych cest k souborum
     * 
     * @param array $rules
     */
    public function rules(array $rules)
    {
        if (isset($rules['names']))
        {
            foreach ($rules['names'] as $name)
            {
                $this->withName($name);
            }
        }

        if (isset($rules['extensions']))
        {
            foreach ($rules['extensions'] as $ext)
            {
                $this->withExtension($ext);
            }
        }

        if (isset($rules['paths']))
        {
            foreach ($rules['paths'] as $path)
            {
                $this->withPath($path);
            }
        }

        if (isset($rules['excludes']))
        {
            foreach ($rules['excludes'] as $exclude)
            {
                $this->excludePath($exclude);
            }
        }
    }

    /**
     * With name
     * 
     * @param string $name
     * @return self
     */
    public function withName($name)
    {
        $this->name[] = $name;
        return $this;
    }

    /**
     * With extension
     * 
     * @param string $extension
     * @return self
     */
    public function withExtension($extension)
    {
        $this->extension[] = $extension;
        return $this;
    }

    /**
     * With path
     * 
     * @param string $path path to the folder with files (without a trailing slash)
     * @return self
     */
    public function withPath($path)
    {
        $this->paths[] = $this->normalizePath($path);
        return $this;
    }

    /**
     * Exclude path
     * 
     * @param string $path exclude path to the folder (without a trailing slash)
     * @return self
     */
    public function excludePath($path)
    {
        $this->exclude[] = $this->normalizePath($path);
        return $this;
    }

    /**
     * Set max depth
     * 
     * @param int $maxDepth
     * @return self
     */
    public function setMaxDepth($maxDepth)
    {
        $this->maxDepth = $maxDepth;
        return $this;
    }

    /**
     * Browse paths and find files
     * 
     * @return self
     */
    public function find()
    {
        $it = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->root, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST);
        $it->setMaxDepth($this->maxDepth);

        foreach ($it as $fileinfo)
        {
            if ('.' !== substr($it->getSubPath(), 0, 1) && $fileinfo->isFile() && $this->validateFile($fileinfo))
            {
                $this->files[] = $fileinfo->getPathname();
            }
        }

        return $this;
    }

    /**
     * Validate file
     * 
     * @param SplFileInfo $fileinfo
     * @return bool
     */
    private function validateFile(SplFileInfo $fileinfo)
    {
        $state = [];

        // validate filename
        if (count($this->name) > 0)
        {
            $state[] = (false !== $this->strposArr($fileinfo->getFilename(), $this->name) ? true : false);
        }

        // validate extension
        if (count($this->extension) > 0)
        {
            $ext = array_flip($this->extension);
            $state[] = isset($ext[$fileinfo->getExtension()]);
        }

        // validate paths and exclude path
        foreach (['paths', 'exclude'] as $val)
        {
            $paths = $this->{$val};
            if (count($paths) > 0)
            {
                foreach ($paths as $path)
                {
                    $pattern = "~" . $this->normalizePath($this->root . DIRECTORY_SEPARATOR . $path) . "~";
                    $result = preg_match($pattern, $this->normalizePath($fileinfo->getPathname()));
                    $state[] = (bool) ('exclude' === $val ? !$result : $result);
                }
            }
        }

        return !in_array(false, $state, true);
    }

    /**
     * Get found files
     * 
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * StrPos for array
     * 
     * @internal
     * @param string $haystack
     * @param array $needles
     * @return int|bool
     */
    private function strposArr($haystack, $needles)
    {
        foreach ((array) $needles as $needle)
        {
            $pos = strpos($haystack, $needle);
            if ($pos !== false)
            {
                return $pos;
            }
        }

        return false;
    }

    /**
     * Normalize a path
     *
     * @author ShiraNai7 <shira.cz>
     * @param string $basePath
     * @param string $path
     * @return string
     */
    public function normalizePath($basePath, $path = '')
    {
        $basePath = str_replace('\\', '/', $basePath);
        $path = str_replace('\\', '/', $path);

        return '' === $path ? $basePath : ($this->isAbsolutePath($path) ? $path : $basePath . '/' . $path );
    }

    /**
     * See if a path is absolute
     *
     * @author ShiraNai7 <shira.cz>
     * @param string $path
     * @return bool
     */
    private function isAbsolutePath($path)
    {
        return (!isset($path[0]) || '/' !== $path[0]) && (!isset($path[1]) || ':' !== $path[1]);
    }

}
