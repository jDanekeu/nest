<?php

namespace Nest;

use Sunlight\Util\Url;

/**
 * Nest
 *
 * @author jDaněk <jdanek.eu>
 */
class Core extends Singleton
{

    /** @var array */
    public static $config = [];

    /** @var array */
    private $groups = [];

    /** @var array */
    private $eggs = [];

    /** @deprecated since version 8.0.1 */
    public static $eggLoader;

    /**
     * Singleton class
     */
    protected function __construct()
    {
        parent::__construct();

        // zpetna kompatibilita s eggy
        self::$eggLoader = &$this;
    }

    /**
     * Vraci sestavenou url cast eggu (př.: module&m=eggname)
     *
     * @param string $egg_id   id eggu
     * @param bool   $without_index vracet url bez "index.php?p="
     * @return string
     */
    public static function composeEggUrl($egg_id, $without_index = false)
    {
        return (false === $without_index ? "./index.php?p=" : "") . "nest&m=" . $egg_id;
    }

    /**
     * Kontrola pristupove urovne
     *
     * @param int $min_level
     * @return bool
     */
    public static function hasRights($min_level)
    {
        return _priv_level >= (10001 < $min_level ? 10001 : $min_level);
    }

    /**
     * 
     * @param array $eggs
     */
    public function addEggs(array $eggs)
    {
        $this->eggs = array_merge($this->eggs, $eggs);
    }

    /**
     * 
     * @return array
     */
    public function getEggs()
    {
        return $this->eggs;
    }

    /**
     * 
     * @param array $groups
     */
    public function addGroups(array $groups)
    {
        $this->groups = array_merge($this->groups, $groups);
    }

    /**
     * 
     * @return array
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Kontrola existence eggu
     *
     * @param string $name
     * @return bool
     */
    public function eggExists($name)
    {
        return isset($this->eggs[$name]);
    }

    /**
     * Ziskani instance aktualniho eggu
     *
     * @return Egg|null
     */
    public function getCurrentEgg()
    {
        $url = Url::getCurrent();

        if (isset($url->query['m']))
        {
            return $this->getEgg($url->query['m']);
        }
        return null;
    }

    /**
     * Ziskani instance eggu podle identifieru
     * 
     * @param string $identifier
     * @return Egg|null
     */
    public function getEgg($identifier)
    {
        if (isset($this->eggs[$identifier]))
        {
            $properities = $this->eggs[$identifier];
            $egg = new Egg\Egg($properities);

            if (null !== $properities['group_id'])
            {
                $egg->group_name = $this->groups[$properities['group_id']]['name'];
            }

            return $egg;
        }
        return null;
    }

}
