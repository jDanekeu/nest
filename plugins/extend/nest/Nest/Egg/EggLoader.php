<?php

namespace Nest\Egg;

use Nest\Core;
use Nest\FileFinder;
use Sunlight\Util\Json;
use Sunlight\Option\OptionSet;

/**
 * Egg Loader
 *
 * @author jDaněk <jdanek.eu>
 */
class EggLoader
{

    const TYPE_EGG = 'egg';
    const TYPE_GROUP = 'group';

    /** @var array */
    public $paths = [];

    /** @var array */
    public $groups_list = [];

    /** @var array */
    public $eggs_list = [];

    /** @var array */
    private $egg_options = [
        'name'         => ['type' => 'array', 'required' => true],
        'description'  => ['type' => 'array', 'required' => false, 'default' => ''],
        'priority'     => ['type' => 'integer', 'required' => false, 'default' => 1],
        'access_level' => ['type' => 'integer', 'required' => false, 'default' => 10001],
        'script'       => ['type' => 'string', 'required' => false],
        'privileges'   => ['type' => 'array', 'required' => false, 'default' => ['create' => true, 'edit' => true, 'confirm' => true, 'delete' => true]],
    ];

    /**
     * @param array $paths
     */
    public function __construct(array $paths)
    {
        $this->find($paths);
        $this->load();
    }

    /**
     * Nalezeni eggu
     * 
     * @param array $paths
     */
    private function find(array $paths)
    {
        foreach ($paths as $path)
        {
            $this->paths = array_merge($this->paths, (new FileFinder($path, [
                'names'      => ['egg', 'group'],
                'extensions' => ['json'],
                    ], true))->getFiles());
        }
    }

    /**
     * Nacteni eggu
     */
    private function load()
    {
        $errors = [];

        $optionSet = new OptionSet($this->egg_options);
        $optionSet->setIgnoreExtraIndexes(true)
                ->addKnownIndexes($optionSet->getIndexes());

        $egg_types = [self::TYPE_GROUP, self::TYPE_EGG];
        foreach ($egg_types as $egg_type)
        {
            foreach ($this->paths as $egg_path)
            {
                $is_egg = false;

                if (1 === preg_match("~{$egg_type}\.json~", $egg_path))
                {
                    // identify egg
                    $is_egg = (self::TYPE_EGG === $egg_type ? true : false);

                    // load options
                    try {
                        $options = Json::decode(file_get_contents($egg_path));
                    } catch (\RuntimeException $e) {
                        $options = null;
                        $errors[] = sprintf('could not parse "' . $egg_type . '.json" - %s', $e->getMessage());
                    }

                    // validate options
                    if (null !== $options)
                    {
                        $optionSet->process($options, $errors);

                        // check rights
                        if (false === $is_egg || (true === $is_egg && true === Core::hasRights($options['access_level'])))
                        {
                            $ids = $this->explodePath(($path = dirname($egg_path)));
                            $identifier = $this->getIdentifier($ids);
                            $error_icon = "<img src='" . _nestroot . "Resources/images/icons/brick_error.png' class='icon'>";
                            $name = isset($options['name'][_language]) ? $options['name'][_language] : $error_icon . $ids[1];
                            $description = isset($options['description'][_language]) ? $options['description'][_language] : '';

                            $data = [];
                            $data['id'] = $ids[1];
                            $data['full_id'] = $identifier;
                            $data['name'] = $name;
                            $data['priority'] = $options['priority'];
                            $data['path'] = $path . DIRECTORY_SEPARATOR;

                            if (true === $is_egg)
                            {
                                // egg description
                                $data['description'] = $description;

                                // group id for egg
                                $data['group_id'] = null;
                                if (true === $this->groupExists($ids[0]))
                                {
                                    $data['group_id'] = $ids[0];
                                    $data['group_name'] = $this->groups_list[$ids[0]]['name'];
                                }

                                // egg script file
                                $data['script'] = $this->scriptFileExists($path, $options['script']);

                                // action privileges
                                $data['privileges'] = array_merge($this->egg_options['privileges']['default'], $options['privileges']);
                            }

                            $list = "{$egg_type}s_list";
                            $this->{$list}[$identifier] = $data;
                        }
                    }
                }
            }
        }
        // sorting
        uasort($this->eggs_list, [__CLASS__, 'prioritySorting']);
        uasort($this->groups_list, [__CLASS__, 'prioritySorting']);
    }

    /**
     * Rozdeleni cesty a ziskani posledni a predposledni slozky
     *
     * @param string $path
     * @return array
     */
    private function explodePath($path)
    {
        $parts = explode('/', str_replace('\\', '/', $path));
        $last = array_pop($parts);
        $penultimate = array_pop($parts);

        return [$penultimate, $last];
    }

    /**
     * Sestaveni identifikatoru skupiny a eggu
     *
     * @param array $args
     * @return string
     */
    private function getIdentifier(array $args)
    {
        if (false === $this->groupExists($args[0]))
        {
            unset($args[0]);
        }

        return implode('/', $args);
    }

    /**
     * Kontrola existence skupiny
     *
     * @param string $name
     * @return bool
     */
    public function groupExists($name)
    {
        return isset($this->groups_list[$name]);
    }

    /**
     * Kontrola existence eggu
     *
     * @param string $name
     * @return bool
     */
    public function eggExists($name)
    {
        return isset($this->eggs_list[$name]);
    }

    /**
     * Kontrola existence souboru se skriptem
     *
     * @param string      $path     (bez koncoveho lomitka)
     * @param string|null $filename
     */
    private function scriptFileExists($path, $filename)
    {
        $script_file = $path . DIRECTORY_SEPARATOR . $filename;
        if (is_file($script_file) && file_exists($script_file))
        {
            return $script_file;
        }
        return null;
    }

    /**
     * [CALLBACK] Serazeni modulu podle priority
     *
     * @param array $a
     * @param array $b
     * @return int
     */
    public static function prioritySorting($a, $b)
    {
        if ($a['priority'] === $b['priority'])
            return 0;
        if ($a['priority'] < $b['priority'])
            return 1;
        return -1;
    }

    /**
     * Vraci vsechny eggy
     *
     * @return array
     */
    public function getEggs()
    {
        return $this->eggs_list;
    }

    /**
     * Vraci vsechny skupiny
     *
     * @return array
     */
    public function getGroups()
    {
        return $this->groups_list;
    }

}
