<?php

namespace Nest\Egg;

use ArrayAccess;
use Nest\Core;

/**
 * Egg class
 *
 * @author jDaněk <jdanek.eu>
 */
class Egg implements ArrayAccess
{

    /** @var array */
    private $properties = [];

    /**
     * Construktor
     */
    public function __construct(array $properties = [])
    {
        $this->properties = array_merge($this->properties, $properties);
    }

    /**
     * Overeni opravneni
     * 
     * @param bool|int $priv
     * @return bool
     */
    public function hasPrivilege($priv)
    {
        if (isset($this->properties['privileges'][$priv]))
        {
            $privilege = $this->properties['privileges'][$priv];
            if (true === is_bool($privilege))
            {
                return $privilege;
            }
            else
            {
                return Core::hasRights($privilege);
            }
        }
        return true;
    }

    /**
     * Setter
     *
     * @param string $name
     * @param mixed  $value
     */
    public function __set($name, $value)
    {
        $this->properties[$name] = $value;
    }

    /**
     * Getter
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        if (isset($this->properties[$name]))
        {
            return $this->properties[$name];
        }
    }

    /**
     * Isset
     *
     * @param string $name
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->properties[$name]);
    }

    /**
     * ArrayAccess methods
     */

    /**
     * Zpracovat nastaveni prvku
     *
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset))
        {
            $this->properties[] = $value;
        }
        else
        {
            $this->properties[$offset] = $value;
        }
    }

    /**
     * Zpracovat test existence klice
     *
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->properties[$offset]);
    }

    /**
     * Zpracovat smazani prvku
     *
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetUnset($offset)
    {
        unset($this->properties[$offset]);
    }

    /**
     * Zpracovat ziskani klice
     *
     * @param mixed $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->properties[$offset]) ? $this->properties[$offset] : null;
    }

}
