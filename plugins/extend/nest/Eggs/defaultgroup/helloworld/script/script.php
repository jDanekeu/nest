
<?php

use Nest\Core as NestCore;
use Nest\Bread\BreadPlus;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\ValidationException;
use Sunlight\Util\Url;
use Sunlight\LangPack;

/* --- kontrola jadra --- */
if (!defined('_root'))
    die;

/**
 * Ukazkovy zakladni Nest Egg
 */
class EggClass extends BreadPlus
{

    /** @ var Egg\Egg */
    public $current_egg;

    /**
     * Nastaveni instance eggu
     */
    protected function setup()
    {
        global $_lang;

        $this->current_egg = NestCore::getInstance()->getCurrentEgg();

        // registrace jazykoveho balicku (pristupny pres "egg.nazeveggu")
        LangPack::register('egg.' . $this->current_egg->id, $this->current_egg->path . 'languages/');

        /**
         * ==========================
         * Nastaveni instance eggu
         * ==========================
         */
        $this->module = NestCore::composeEggUrl($this->current_egg->full_id, true);
        $this->table = 'users';
        $this->path = __DIR__;

        // PHP 5.3 closure fix
        $that = $this;

        /* vypis */
        $this->actions['list']['title'] = '%s - ' . $_lang['nest']['egg.active.list'];
        $this->actions['list']['columns'][] = 't.id, t.username, t.email';
        $this->actions['list']['paginator_size'] = 15;
        $this->actions['list']['query_orderby'] = 't.id DESC';

        // bread plus
        $this->actions['list']['callback'] = [__CLASS__, 'listAction'];
        $this->actions['list']['handler'] = [$this, 'listHandler'];

        /* pridavani */
        $this->actions['create']['title'] = '%s - ' . $_lang['nest']['egg.active.create'];
        $this->actions['create']['template'] = 'edit';
        $this->actions['create']['initial_data'] = [
            'username' => '',
            'email'    => '@'
        ];
        $this->actions['create']['handler'] = [$this, 'handler'];

        /* mazaní */
        $this->actions['del']['title'] = '%s - ' . $_lang['nest']['egg.active.del'];
        $this->actions['del']['extra_columns'][] = 't.username';

        /* editace */
        $this->actions['edit']['title'] = '%s - ' . $_lang['nest'] ['egg.active.edit'];
        $this->actions['edit']['handler'] = [$this, 'handler'];

        /* confirm */
        $this->actions['confirm']['title'] = '%s - ' . $_lang['nest']['egg.active.accept'];
        $this->actions['confirm']['callback'] = [__CLASS__, 'switchAction'];
        $this->actions['confirm']['changes'] = ['confirm' => 1];
        $this->actions['confirm']['extra_columns'][] = 't.confirm, t.username';

        /**
         * ===========================
         * Nastaveni titulku šablonám
         * ===========================
         */
        foreach (array_keys($this->actions) as $action_name)
        {
            $this->actions[$action_name]['on_before'] = function(&$params) use($that) {
                $params['item_name'] = $that->current_egg->name;
            };
        }
    }

    /**
     * Action handler
     *
     * @param array $args
     * @return boolean|array
     */
    public function handler($args)
    {
        // validovat odesilana data metodou
        $validateResult = $this->validate($_POST, $args['create']);
        $errors = [];
        do
        {
            // pokud jsou ve zpracovani chyby ... zastavit zpracovani
            if (sizeof($validateResult[0]) > 0)
            {
                $errors[] = [2, _msgList($validateResult[0], 'errors'), null, true];
                break;
            }

            // vse ok?
            $args['success'] = true;
            return $validateResult[1];
        } while (false);
        return $errors;
    }

    /**
     * Validace dat odeslanych sablonymi
     *
     * @param array $postData   $_POST data sablon
     * @param bool  $createMode prepinac create/edit modu, lze validaci zacilit jen na create nebo jen edit
     * @return array [0=> array chyby, 1=> array zpracovana data]
     */
    function validate(array $postData, $createMode = false)
    {
        // log chyb
        $errors = [];

        try {

            v::create()
                    ->key('username', v::string()->notEmpty())
                    ->key('email', v::string()->length(6, null))
                    ->assert($postData);
        } catch (ValidationException $exception) {

            $errors = $exception->findMessages([
                'username' => 'pole {{name}} nesmí být prázdné',
                    //'publicname'=>  sprintf($_lang['validator']['notEmpty'],'Public jméno'),
                    //'phone'=>  sprintf($_lang['validator']['notEmpty'],'Telefonní číslo'),
            ]);
        }

        // zpracovana data
        $validatedData = [
            'username' => $postData['username'],
            'email'    => $postData['email'],
        ];

        return [$errors, $validatedData];
    }

    /**
     * BreadPlus listHandler
     * @param boolean $args
     * @return boolean|array
     */
    public function listHandler($args)
    {
        $errors = [];

        // kod handleru
        //...  
        // zapis chyb
        //$errors[]='Lorem Ipsum';
        // return handleru
        $errors = array_filter($errors);
        return (empty($errors) ? true : $errors);
    }

}

/* --- akce --- */
$eggClass = new EggClass();
return $eggClass->run();
