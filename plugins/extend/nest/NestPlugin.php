<?php

namespace SunlightPlugins\Extend\Nest;

use Sunlight\Extend;
use Sunlight\Plugin\ExtendPlugin;
use Nest\Core as NestCore;
use Nest\FileFinder;
use Nest\Egg\EggLoader;
use Nest\Template\Template;

/**
 * Nest plugin
 *
 * @author jDaněk <jdanek.eu>
 */
class NestPlugin extends ExtendPlugin
{

    public $egg_paths = [];

    public function init()
    {
        if (true === $this->isInstalled())
        {
            $basePath = $this->getWebPath() . "/";
            $this->egg_paths[] = $basePath . "Eggs/";

            // konstanta
            define('_nestroot', $basePath);

            Template::$cache_directory = _root . 'system/cache/nest/';

            // inicializace jadra
            if (defined('_nest_use_default_egg') && 1 == _nest_use_default_egg)
            {
                NestCore::$config['default_egg'] = _nest_default_egg;
            }

            Extend::call('nest.remote.egg', ['list' => &$this->egg_paths]);

            $eggLoader = new EggLoader($this->egg_paths);

            $nest = NestCore::getInstance();
            $nest->addEggs($eggLoader->getEggs());
            $nest->addGroups($eggLoader->getGroups());
        }
    }

    /**
     * Integrace Nest do menu
     * @global array $_lang
     * @param array $args
     */
    public function onMenu(array $args)
    {
        if (true === $this->isInstalled())
        {
            $args['modules']['nest'] = [
                'title'      => _nest_tab_title,
                'access'     => '_priv_nestaccess',
                'script'     => $this->getWebPath() . '/Resources/pages/dashboard.php',
                'menu'       => true,
                'menu_order' => _nest_tab_position,
            ];
        }
    }

    /**
     * Registrace privilegia
     * 
     * @param array $args
     */
    public function sysPrivileges($args)
    {
        if (true === $this->isInstalled())
        {
            $args['privileges'] = array_merge($args['privileges'], [
                'nestaccess'
            ]);
        }
    }

    /**
     * Pridani privilegia do nastaveni skupin
     * 
     * @global array $_lang
     * @param array $args
     */
    public function adminEditgroupRights($args)
    {
        if (true === $this->isInstalled())
        {
            global $_lang;
            $args['rights'][] = [
                'title'  => $_lang['nest']['priv.title'],
                'rights' => [
                    [
                        'name'      => 'nestaccess',
                        'label'     => $_lang['nest']['priv.nestaccess'],
                        'help'      => $_lang['nest']['priv.nestaccess.help'],
                        'dangerous' => true,
                    ],
                ],
            ];
        }
    }

    /**
     * Nacteni CSS a JS
     *
     * @param array $args
     */
    public function onHead(array $args)
    {
        if (true === $this->isInstalled())
        {
            $basePath = $this->getWebPath() . "/";

            // autoload egg CSS a JS
            foreach ($this->egg_paths as $path)
            {
                $args['css'] = array_merge($args['css'], (new FileFinder($path, ['extensions' => ['css']], true))->getFiles());
                $args['js'] = array_merge($args['js'], (new FileFinder($path, ['extensions' => ['js']], true))->getFiles());
            }

            $args['css']['nest'] = $basePath . 'Resources/css/style.css';
            $args['css']['nest_accordion_menu'] = $basePath . 'Resources/css/accordion_menu.css';
            $args['js'][] = $basePath . 'Resources/js/checkall.js';
            $args['js'][] = $basePath . 'Resources/js/jquery.cookie.js';
            $args['js'][] = $basePath . 'Resources/js/cookies.js';
        }
    }

    /**
     * Generovani admin CSS
     *
     * @param array $args
     */
    public function onAdminStyle(array $args)
    {
        if (true === $this->isInstalled())
        {
            $args['output'] .= ".topborder{border-top: 1px solid {$GLOBALS['scheme_smoke']}; padding-bottom: 5px; margin-top: 12px;}\n";

            $args['output'] .= "/* Nest Accordion Menu */\n";

            $args['output'] .= ".nest-menu ul li label {\n";
            $args['output'] .= "background: {$GLOBALS['scheme']}; /* fallback colour */\n";
            $args['output'] .= "color: {$GLOBALS['scheme_bar_text']};\n";
            $args['output'] .= "text-shadow: 0 1px 1px {$GLOBALS['scheme_bar_shadow']};\n";
            $args['output'] .= "letter-spacing: 0.09em;\n";
            $args['output'] .= "background-color: {$GLOBALS['scheme_bar']};";
            $args['output'] .= "}\n";

            $args['output'] .= ".nest-menu ul ul li a {\n";
            $args['output'] .= "display:block;;\n";
            $args['output'] .= "padding:6px 12px;\n";
            $args['output'] .= "color: {$GLOBALS['scheme_link']};\n";
            $args['output'] .= "text-decoration:none;\n";
            $args['output'] .= "}\n";

            $args['output'] .= ".nest-menu ul ul li a:hover {\n";
            $args['output'] .= "color: {$GLOBALS['scheme_text']};\n";
            $args['output'] .= "}\n";

            $args['output'] .= "/* Nest eggs templates */\n";

            $args['output'] .= ".nest-content table.list tbody tr:hover td {\n";
            $args['output'] .= "background-color: {$GLOBALS['scheme_smoke_lighter']};\n";
            $args['output'] .= "}\n";

            $args['output'] .= "/* Classic Theme */\n";
            $args['output'] .= ".nest-wrapper > a{margin: 5px;}\n";
            $args['output'] .= ".egg-container{float: left; width: 260px;}\n";
            $args['output'] .= "img.egg-icon { float: left; padding: 0px 8px 0px 0px; width: 48px; height: 48px;}\n";
            $args['output'] .= ".egg-data {display: block; float: left; padding: 4px 10px 0 0;}\n";
            $args['output'] .= ".egg-title {color: {$GLOBALS['scheme_link']};font-size: 17px; display: block; padding-bottom: 5px;}\n";
            $args['output'] .= ".egg-description {color: {$GLOBALS['scheme_text']}; display: block;}\n";
        }
    }

    /**
     * Pridani Nest nastaveni do nastaveni systemu 
     * 
     * @global array $_lang
     * @param array $args
     */
    public function nestSettings(array $args)
    {
        global $_lang;

        if (true === $this->isInstalled())
        {
            $args['editable']['nest_env'] = [
                'title' => "[NEST Plugin]",
                'items' => [
                    [
                        'name'  => 'nest_tab_title',
                        'label' => $_lang['nest']['settings.nest_tab_title'],
                        'help'  => $_lang['nest']['settings.nest_tab_title.help']
                    ],
                    [
                        'name'        => 'nest_tab_position',
                        'input_class' => 'inputsmall',
                        'label'       => $_lang['nest']['settings.nest_tab_position'],
                        'help'        => $_lang['nest']['settings.nest_tab_position.help']
                    ],
                    [
                        'name'    => 'nest_theme',
                        'choices' => ['foreight' => 'For Eight', 'classic' => 'Classic'],
                        'label'   => $_lang['nest']['settings.nest_theme'],
                        'help'    => $_lang['nest']['settings.nest_theme.help']
                    ],
                    [
                        'name'  => 'nest_use_default_egg',
                        'label' => $_lang['nest']['settings.nest_use_default_egg'],
                        'help'  => $_lang['nest']['settings.nest_use_default_egg.help']
                    ],
                    [
                        'name'  => 'nest_default_egg',
                        'label' => $_lang['nest']['settings.nest_default_egg'],
                        'help'  => $_lang['nest']['settings.nest_default_egg.help']
                    ],
            ]];
        }
    }

}
