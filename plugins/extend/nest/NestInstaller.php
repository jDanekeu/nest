<?php

namespace SunlightPlugins\Extend\Nest;

use Sunlight\Plugin\PluginInstaller;

/**
 * NestInstaller
 *
 * @author jDaněk <jdanek.eu>
 */
class NestInstaller extends PluginInstaller
{

    protected function doInstall()
    {
        $this->loadSqlDump(__DIR__ . '/Resources/install/install.sql');
    }

    protected function doUninstall()
    {
        $this->loadSqlDump(__DIR__ . '/Resources/install/uninstall.sql');
    }

    protected function verify()
    {
        $columns = $this->checkColumns(_prefix . "groups", ['nestaccess']);
        return (0 == sizeof($columns));
    }

}
