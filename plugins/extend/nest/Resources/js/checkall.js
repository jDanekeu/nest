function checkAll(formName, checkToggle)
{
    var checkboxes = document.forms[formName].querySelectorAll('input');
    for (var i = 0; i < checkboxes.length; i++)
    {

        if (checkboxes[i].type === 'checkbox') {
            checkboxes[i].checked = checkToggle;
        }
    }

}