$(document).ready(function () {

    /**INIT **/
    var cook = $.cookie('nestmenu') || '', checkboxes = $('.nest-menu input:checkbox');
    cook = (cook === '') ? [] : cook.split(",");

    checkboxes.each(function () {
        var lindex = $.inArray($(this).val().toString(), cook);
        if (lindex > -1) {
            $(this).prop('checked', true);
        }
    });

    /**on change add to cookie**/
    checkboxes.on('change', function () {
        var cook = $.cookie('nestmenu') || '',
                indx = $(this).val().toString();

        cook = (cook === '') ? [] : cook.split(",");
        if (!this.checked) {
            cook = $.grep(cook, function (value) {
                return value != indx;
            });
        } else {
            if ($.inArray(indx, cook))
                cook.push(indx);

        }

        var cook_size = cook.length;
        if (cook_size === 0)
            $.removeCookie('nestmenu');
        else {
            $.cookie('nestmenu', cook.join(","), {expires: 30});    //60days store
        }

        return false;
    });

});
