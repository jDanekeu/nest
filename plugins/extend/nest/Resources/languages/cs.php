<?php

return [
    'description'                        => 'Ve správě portálu jsou dostupné doplňkové funkce systému. Přístup k jednotlivým modulům závisí na Vašich právech.',
    /* ========== settings ========== */
    'settings.p'                         => '*DESC*',
    'settings.yes'                       => 'Ano',
    'settings.no'                        => 'Ne',
    'settings.env'                       => 'Prostředí',
    'settings.nest_tab_title'            => 'Titulek v záložce',
    'settings.nest_tab_title.help'       => 'Změna názvu záložky <em>(př.: Správa portálu)</em>',
    'settings.nest_theme'                => 'Motiv',
    'settings.nest_theme.help'           => 'Změna motivu prostředí<br><em>při použití motivu <strong>Classic</strong> je nutné deaktivovat výchozí egg</em>',
    'settings.nest_tab_position'         => 'Pozice záložky',
    'settings.nest_tab_position.help'    => 'Pozice záložky v systémovém menu',
    /* ========== settings-egg ========== */
    'settings.eggs'                      => 'Eggy',
    'settings.nest_use_default_egg'      => 'Výchozí egg',
    'settings.nest_use_default_egg.help' => 'používat výchozí egg',
    'settings.nest_default_egg'          => '',
    'settings.nest_default_egg.help'     => 'egg který bude zobrazen při vstupu do rozhraní <em>(př.: defaultgroup/helloworld)</em>',
    /* ========== egg ========== */
    'bread.filter.active'                => 'Zobrazené záznamy jsou nyní filtrovány, pro zobrazení všech záznamů je nutné filtr(y) zrušit',
    'bread.filter.empty.whitelist'       => 'Egg nemá vyplněn whitelist povolených sloupců, proto je filtrování VYPNUTO',
    'bread.filter.whitelist'             => 'Filtrování sloupcem `%s` není povoleno whitelistem eggu',
    'egg.requireVersion'                 => 'Egg vyžaduje ',
    'egg.notExists'                      => 'Egg `*module_id*` ke kterému se snažíte přistoupit neexistuje nebo nemáte dostatečné oprávnění.',
    'egg.author'                         => 'Autor',
    'egg.email'                          => 'Email',
    'egg.url'                            => 'Url',
    'egg.active.list'                    => 'záznamy',
    'egg.active.create'                  => 'nový záznam',
    'egg.active.del'                     => 'odstranit záznam',
    'egg.active.edit'                    => 'upravit záznam',
    'egg.active.accept'                  => 'potvrdit záznam',
    'egg.active.denied'                  => 'zamítnout záznam',
    /* ========== privileges ========== */
    'priv.title'                         => 'Rozhraní NEST',
    'priv.nestaccess'                    => 'Pracovat s NESTem',
    'priv.nestaccess.help'               => 'povolit přístup do rozhraní',
    'priv.nestaccess2'                   => 'Přístup do nastavení',
    'priv.nestaccess2.help'              => 'povolit přístup do nastavení rozhraní',
];
