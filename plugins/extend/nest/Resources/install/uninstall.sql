ALTER TABLE `sunlight_groups`
  DROP `nestaccess`;

DELETE FROM `sunlight_settings` WHERE `sunlight_settings`.`var` = 'nest_tab_title';
DELETE FROM `sunlight_settings` WHERE `sunlight_settings`.`var` = 'nest_theme';
DELETE FROM `sunlight_settings` WHERE `sunlight_settings`.`var` = 'nest_tab_position';
DELETE FROM `sunlight_settings` WHERE `sunlight_settings`.`var` = 'nest_use_default_egg';
DELETE FROM `sunlight_settings` WHERE `sunlight_settings`.`var` = 'nest_default_egg';