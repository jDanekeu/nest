ALTER TABLE `sunlight_groups`  ADD `nestaccess` TINYINT(1) NOT NULL DEFAULT '0';

UPDATE `sunlight_groups` SET 
`nestaccess` = '1'
WHERE `sunlight_groups`.`id` = 1;

INSERT INTO `sunlight_settings` (`var`, `val`, `format`, `constant`, `preload`, `web`, `admin`) 
VALUES ('nest_tab_title', 'Nest 8', NULL, '1', '1', '0', '1'),
       ('nest_theme', 'foreight', NULL, '1', '1', '0', '1'),
       ('nest_tab_position', '15', 'int', '1', '1', '0', '1'),
       ('nest_use_default_egg', '1', 'bool', '1', '1', '0', '1'),
       ('nest_default_egg', 'defaultgroup/helloworld', NULL, '1', '1', '0', '1');